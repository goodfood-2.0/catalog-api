package migrations

import (
	Models "catalog-api/models"

	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&Models.Ingredient{},
		&Models.Product{},
		&Models.ProductIngredient{},
	)
}
