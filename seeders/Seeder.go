package seeders

import (
	"log"
)

func Seed() {
	err := SeedProducts()
	if err != nil {
		log.Fatalf("Failed to seed products: %s", err)
	}
}
