package seeders

import (
	database "catalog-api/db"
	"catalog-api/models"
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type IngredientSeed struct {
	Name                  string `json:"name"`
	IsAllergen            bool   `json:"is_allergen"`
	IsAllergenDescription string `json:"is_allergen_description"`
	RequiredQuantity      int    `json:"required_quantity"`
}

type ProductSeed struct {
	Name         string            `json:"name"`
	Description  string            `json:"description"`
	Price        float64           `json:"price"`
	Activated    bool              `json:"activated"`
	Image        string            `json:"image"`
	IDRestaurant int               `json:"id_restaurant"`
	Ingredients  []*IngredientSeed `json:"ingredients"`
}

func SeedProducts() error {
	// Connect to the database
	db, err := database.Connect()
	if err != nil {
		return fmt.Errorf("failed to connect to the database: %s", err)
	}

	// Check if data already exists
	var count int64
	err = db.Model(&models.Product{}).Count(&count).Error
	if err != nil {
		return fmt.Errorf("failed to count Products: %s", err)
	}

	if count > 0 {
		log.Println("Products already exist, skipping seed")
		return nil
	}

	// Load data from JSON file
	file, err := os.Open("seeds/products.json")
	if err != nil {
		return fmt.Errorf("failed to open file: %s", err)
	}
	defer file.Close()

	var productsData []ProductSeed
	err = json.NewDecoder(file).Decode(&productsData)
	if err != nil {
		return fmt.Errorf("failed to decode JSON: %s", err)
	}

	// Iterate through each product data and create products
	for _, productData := range productsData {
		// Create product
		product := &models.Product{
			Name:         productData.Name,
			Description:  productData.Description,
			Price:        productData.Price,
			Activated:    productData.Activated,
			Image:        productData.Image,
			IDRestaurant: productData.IDRestaurant,
		}

		// Create the product in the database
		db.Create(product)

		// Create ingredients and relationships
		for _, ingredientData := range productData.Ingredients {
			// Check if ingredient with the same name already exists
			var existingIngredient models.Ingredient
			var count int64
			err = db.Model(existingIngredient).Where("name = ?", ingredientData.Name).Count(&count).Error
			if err != nil {
				return fmt.Errorf("error checking existing ingredient: %s", err)
			}

			if count == 0 {
				// Ingredient doesn't exist, create a new one
				ingredient := &models.Ingredient{
					Name:                  ingredientData.Name,
					IsAllergen:            ingredientData.IsAllergen,
					IsAllergenDescription: ingredientData.IsAllergenDescription,
				}

				// Create the ingredient in the database
				if err := db.Create(ingredient).Error; err != nil {
					return fmt.Errorf("error creating ingredient: %s", err)
				}

				// Use the newly created ingredient in the relationship
				existingIngredient = *ingredient
			}

			// Create ProductIngredient relationship
			productIngredient := &models.ProductIngredient{
				IDProduct:        product.ID,
				IDIngredient:     existingIngredient.ID,
				RequiredQuantity: ingredientData.RequiredQuantity,
			}

			// Create record in the database
			if err := db.Create(productIngredient).Error; err != nil {
				return fmt.Errorf("error creating productIngredient: %s", err)
			}
		}
	}

	log.Println("Products seeded successfully")
	return nil
}
