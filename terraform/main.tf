terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.48.0"
    }
  }

  backend "azurerm" {

  }
}

provider "azurerm" {
  features {

  }
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-${var.project_name}-${var.environment_suffix}"
  location = var.location
}

####################
# DATABASE SECTION #
####################

resource "azurerm_postgresql_server" "postgres-server" {
  name                = "postgres-server-${var.project_name}-${var.environment_suffix}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  sku_name = "B_Gen5_2"

  storage_mb                   = 5120
  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true

  administrator_login              = "postgres"      # data.azurerm_key_vault_secret.database-login.value
  administrator_login_password     = "Password2023!" # data.azurerm_key_vault_secret.database-password.value
  version                          = "9.5"
  ssl_enforcement_enabled          = false
  ssl_minimal_tls_version_enforced = "TLSEnforcementDisabled"
}

resource "azurerm_postgresql_firewall_rule" "firewall" {
  name                = "firewall-${var.project_name}${var.environment_suffix}"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_postgresql_server.postgres-server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

#####################
# CONTAINER SECTION #
#####################

resource "azurerm_container_group" "container_group" {
  name                = "container-${var.project_name}-${var.environment_suffix}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  ip_address_type     = "Public"
  dns_name_label      = "container-${var.project_name}-${var.environment_suffix}"
  os_type             = "Linux"

  container {
    name   = "${var.project_name}-${var.environment_suffix}"
    image  = "registry.gitlab.com/goodfood-2.0/${var.project_name}/${var.environment_suffix}"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 80
      protocol = "TCP"
    }

    environment_variables = {
      "DB_HOST"     = azurerm_postgresql_server.postgres-server.fqdn
      "DB_PORT"     = "5432"
      "DB_NAME"     = "postgres"
      "DB_USERNAME" = "${azurerm_postgresql_server.postgres-server.administrator_login}@${azurerm_postgresql_server.postgres-server.name}"
      "DB_PASSWORD" = azurerm_postgresql_server.postgres-server.administrator_login_password
    }
  }
}