package tests

import (
	"bytes"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"catalog-api/controllers"
	"catalog-api/models"
)

func TestGetAllProductsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductsController avec le MockDB
	controller := &controllers.ProductController{DB: mockDB.DB}

	// Créer une requête HTTP simulée
	req, err := http.NewRequest("GET", "/products?page=1&limit=10", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Appeler la méthode GetAllProductsHandler
	controller.GetAllProductsHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	expected := `{"count":0,"data":[],"limit":10,"page":1}`
	assert.Equal(t, expected, strings.TrimSpace(w.Body.String()))
}

func TestGetProductsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductsController avec le MockDB
	controller := &controllers.ProductController{DB: mockDB.DB}

	// Créer une requête HTTP simulée avec un paramètre d'ID
	req, err := http.NewRequest("GET", "/products/1", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode GetProductsHandler
	controller.GetProductHandler(w, req)

	// Vérifier le code de statut HTTP est 500
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	// Vérifier le contenu du corps de la réponse
	expected := `record not found`
	assert.Equal(t, expected, strings.TrimSpace(w.Body.String()))
}

func TestCreateProductsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	// Check if there was an error connecting to the database
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Create an instance of ProductsController with MockDB
	controller := &controllers.ProductController{DB: mockDB.DB}

	// Create a new multipart writer
	var requestBody bytes.Buffer
	writer := multipart.NewWriter(&requestBody)

	// Add form fields
	writer.WriteField("name", "Test Products")

	// Close the writer when done adding fields
	err = writer.Close()
	assert.NoError(t, err)

	// Create a new POST request with the multipart form data
	req, err := http.NewRequest("POST", "/products", &requestBody)
	// Check if there was an error creating the request
	assert.NoError(t, err)

	// Set the Content-Type header to multipart form data
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// Create a ResponseWriter
	w := httptest.NewRecorder()

	// Call the CreateProductHandler method
	controller.CreateProductHandler(w, req)

	// Check the HTTP status code
	assert.Equal(t, http.StatusOK, w.Code)

	// Check the response body for the expected content
	assert.Contains(t, w.Body.String(), `"name":"Test Products"`)
}

func TestUpdateProductsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	assert.NoError(t, err)

	// Insert a record with ID 1 into the mock database
	product := models.Product{Name: "Test Product 1"}
	db.Create(&product)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductsController avec le MockDB
	controller := &controllers.ProductController{DB: mockDB.DB}

	// Create a new multipart writer
	var requestBody bytes.Buffer
	writer := multipart.NewWriter(&requestBody)

	// Add form fields
	writer.WriteField("name", "Test Products 2")

	// Close the writer when done adding fields
	err = writer.Close()
	assert.NoError(t, err)

	// Créer une requête HTTP simulée avec le payload JSON et un ID simulé
	req, err := http.NewRequest("PUT", "/products/1", &requestBody)
	assert.NoError(t, err)

	// Set the Content-Type header to multipart/form-data
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode UpdateProductsHandler
	controller.UpdateProductHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Contains(t, w.Body.String(), `"name":"Test Products 2"`)
}

func TestDeleteProductsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductsController avec le MockDB
	controller := &controllers.ProductController{DB: mockDB.DB}

	// Créer une requête HTTP simulée avec un ID simulé
	req, err := http.NewRequest("DELETE", "/products/1", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode DeleteProductsHandler
	controller.DeleteProductHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Equal(t, "", w.Body.String())
}
