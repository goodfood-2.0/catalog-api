package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"catalog-api/controllers"
)

func TestGetAllIngredientsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de IngredientController avec le MockDB
	controller := &controllers.IngredientController{DB: mockDB.DB}

	// Créer une requête HTTP simulée
	req, err := http.NewRequest("GET", "/ingredients?page=1&limit=10", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Appeler la méthode GetAllIngredientsHandler
	controller.GetAllIngredientsHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	expected := `{"count":0,"data":[],"limit":10,"page":1}`
	assert.Equal(t, expected, strings.TrimSpace(w.Body.String()))
}

func TestGetIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de IngredientController avec le MockDB
	controller := &controllers.IngredientController{DB: mockDB.DB}

	// Créer une requête HTTP simulée avec un paramètre d'ID
	req, err := http.NewRequest("GET", "/ingredients/1", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode GetIngredientHandler
	controller.GetIngredientHandler(w, req)

	// Vérifier le code de statut HTTP est 500
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	// Vérifier le contenu du corps de la réponse
	expected := `record not found`
	assert.Equal(t, expected, strings.TrimSpace(w.Body.String()))
}

func TestCreateIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de IngredientController avec le MockDB
	controller := &controllers.IngredientController{DB: mockDB.DB}

	// Créer une structure Ingredient simulée
	ingredient := map[string]interface{}{
		"name": "Test Ingredient",
	}

	// Convertir la structure en JSON
	payload, err := json.Marshal(ingredient)
	assert.NoError(t, err)

	// Créer une requête HTTP simulée avec le payload JSON
	req, err := http.NewRequest("POST", "/ingredients", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Appeler la méthode CreateIngredientHandler
	controller.CreateIngredientHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Contains(t, w.Body.String(), `"name":"Test Ingredient"`)
}

func TestUpdateIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de IngredientController avec le MockDB
	controller := &controllers.IngredientController{DB: mockDB.DB}

	// Créer une structure d'update simulée
	update := map[string]interface{}{
		"name": "Updated Ingredient",
	}

	// Convertir la structure en JSON
	payload, err := json.Marshal(update)
	assert.NoError(t, err)

	// Créer une requête HTTP simulée avec le payload JSON et un ID simulé
	req, err := http.NewRequest("PUT", "/ingredients/1", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode UpdateIngredientHandler
	controller.UpdateIngredientHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Contains(t, w.Body.String(), `"name":"Updated Ingredient"`)
}

func TestDeleteIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de IngredientController avec le MockDB
	controller := &controllers.IngredientController{DB: mockDB.DB}

	// Créer une requête HTTP simulée avec un ID simulé
	req, err := http.NewRequest("DELETE", "/ingredients/1", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"id": "1"})

	// Appeler la méthode DeleteIngredientHandler
	controller.DeleteIngredientHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Equal(t, "", w.Body.String())
}
