package tests

import (
	"catalog-api/migrations"
	"os"
	"testing"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestMain(m *testing.M) {
	// Instance MockDB
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// Run migrations
	if err := migrations.Migrate(db); err != nil {
		panic("failed to migrate database")
	}

	// Verify if the tables exists
	if !db.Migrator().HasTable("product-ingredients") {
		panic("table product-ingredients does not exist")
	}

	if !db.Migrator().HasTable("products") {
		panic("table products does not exist")
	}

	if !db.Migrator().HasTable("ingredients") {
		panic("table ingredients does not exist")
	}

	// Run tests
	code := m.Run()

	// Exit
	os.Exit(code)
}

type MockDB struct {
	*gorm.DB
}

func (m *MockDB) Find(dest interface{}, conds ...interface{}) *gorm.DB {
	return m.DB
}

func (m *MockDB) Where(query interface{}, args ...interface{}) *gorm.DB {
	return m.DB
}

func (m *MockDB) First(dest interface{}, conds ...interface{}) *gorm.DB {
	return m.DB
}

func (m *MockDB) Create(value interface{}) *gorm.DB {
	return m.DB
}

func (m *MockDB) Save(value interface{}) *gorm.DB {
	return m.DB
}

func (m *MockDB) Delete(value interface{}, conds ...interface{}) *gorm.DB {
	return m.DB
}
