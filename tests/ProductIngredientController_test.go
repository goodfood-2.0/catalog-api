package tests

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"catalog-api/controllers"
)

func TestGetAllProductIngredientsHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductIngredientsController avec le MockDB
	controller := &controllers.ProductIngredientController{DB: mockDB.DB}

	// Créer une requête HTTP simulée
	req, err := http.NewRequest("GET", "/product-ingredients?page=1&limit=10", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Appeler la méthode GetAllProductIngredientsHandler
	controller.GetAllProductIngredientsHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	expected := `{"count":0,"data":[],"limit":10,"page":1}`
	assert.Equal(t, expected, strings.TrimSpace(w.Body.String()))
}

func TestCreateProductIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductIngredientsController avec le MockDB
	controller := &controllers.ProductIngredientController{DB: mockDB.DB}

	// Créer une structure ProductIngredients simulée
	product := map[string]interface{}{
		"name": "Test ProductIngredients",
	}

	// Convertir la structure en JSON
	payload, err := json.Marshal(product)
	assert.NoError(t, err)

	// Créer une requête HTTP simulée avec le payload JSON
	req, err := http.NewRequest("POST", "/product-ingredients/1/ingredient/1", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"product": "1", "ingredient": "1"})

	// Appeler la méthode CreateProductIngredientsHandler
	controller.CreateProductIngredientHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Contains(t, w.Body.String(), `"id_product":1`)
}

func TestDeleteProductIngredientHandler(t *testing.T) {
	// Instance MockDb Sqlite
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})

	// Vérifier si une erreur s'est produite lors de la connexion à la base de données
	assert.NoError(t, err)

	// Instance MockDB
	mockDB := &MockDB{db}

	// Créer une instance de ProductIngredientsController avec le MockDB
	controller := &controllers.ProductIngredientController{DB: mockDB.DB}

	// Créer une requête HTTP simulée avec un ID simulé
	req, err := http.NewRequest("DELETE", "/product-ingredients/1/ingredient/1", nil)
	assert.NoError(t, err)

	// Créer un ResponseWriter simulé
	w := httptest.NewRecorder()

	// Utiliser mux.Vars pour simuler les variables de la requête
	req = mux.SetURLVars(req, map[string]string{"product": "1", "ingredient": "1"})

	// Appeler la méthode DeleteProductIngredientsHandler
	controller.DeleteProductIngredientHandler(w, req)

	// Vérifier le code de statut HTTP
	assert.Equal(t, http.StatusOK, w.Code)

	// Vérifier le contenu du corps de la réponse
	assert.Equal(t, "", w.Body.String())
}
