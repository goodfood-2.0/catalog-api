# catalog-api

## TERRAFORM

### RUNNING TERRAFORM

Execute the following command

```
terraform init -backend-config=".\env\dev-backend.tfvars" -reconfigure
```

## RUNNING THE PROJECT

Execute the following command

```
docker-compose build --no-cache; docker-compose up -d
```

This will build your project's image and then run the containers

(Must implement hot reload)
