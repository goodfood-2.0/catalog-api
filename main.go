package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"

	"catalog-api/azure"
	"catalog-api/controllers"
	database "catalog-api/db"
	"catalog-api/migrations"
	"catalog-api/seeders"

	httpSwagger "github.com/swaggo/http-swagger/v2"
)

// @title API Catalog
// @version 1.10
// @description Listing products and ingredients, with relationship between them.
// @termsOfService http://swagger.io/terms/

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization

//swagger path

// @BasePath /catalog
func main() {

	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}

	// Auto-migrate models
	if err := migrations.Migrate(db); err != nil {
		log.Fatal(err)
	}

	// Seed database
	seeders.Seed()

	router := mux.NewRouter()
	router = router.PathPrefix("/catalog").Subrouter()

	// Initialize Azure Blob Service URL (replace with your actual Azure Storage settings)
	blobServiceURL := azure.InitializeAzureBlobServiceURL()

	productController := &controllers.ProductController{
		DB:             db,
		BlobServiceURL: blobServiceURL,
	}
	router.HandleFunc("/products", productController.GetAllProductsHandler).Methods("GET")
	router.HandleFunc("/products/{id}", productController.GetProductHandler).Methods("GET")
	router.HandleFunc("/products", productController.CreateProductHandler).Methods("POST")
	router.HandleFunc("/products/{id}", productController.UpdateProductHandler).Methods("PUT")
	router.HandleFunc("/products/{id}", productController.DeleteProductHandler).Methods("DELETE")

	ingredientController := &controllers.IngredientController{DB: db}
	router.HandleFunc("/ingredients", ingredientController.GetAllIngredientsHandler).Methods("GET")
	router.HandleFunc("/ingredients/{id}", ingredientController.GetIngredientHandler).Methods("GET")
	router.HandleFunc("/ingredients", ingredientController.CreateIngredientHandler).Methods("POST")
	router.HandleFunc("/ingredients/{id}", ingredientController.UpdateIngredientHandler).Methods("PUT")
	router.HandleFunc("/ingredients/{id}", ingredientController.DeleteIngredientHandler).Methods("DELETE")

	productIngredientController := &controllers.ProductIngredientController{DB: db}
	router.HandleFunc("/product-ingredients", productIngredientController.GetAllProductIngredientsHandler).Methods("GET")
	router.HandleFunc("/product-ingredients/{product}/ingredient/{ingredient}", productIngredientController.CreateProductIngredientHandler).Methods("POST")
	router.HandleFunc("/product-ingredients/{product}/ingredient/{ingredient}", productIngredientController.DeleteProductIngredientHandler).Methods("DELETE")

	// Serve the Swagger JSON
	router.PathPrefix("/swagger").Handler(httpSwagger.Handler(
		httpSwagger.URL("/catalog/docs/swagger.json"),
		httpSwagger.DeepLinking(true),
		httpSwagger.DocExpansion("none"),
		httpSwagger.DomID("swagger-ui"),
	)).Methods(http.MethodGet)

	router.HandleFunc("/docs/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		swaggerJSON, err := ioutil.ReadFile("./docs/swagger.json")
		if err != nil {
			http.Error(w, "Failed to read Swagger JSON file", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(swaggerJSON)
	})

	// Close database on end of main function
	defer database.Close(db)

	// CORS middleware
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})
	corsRouter := handlers.CORS(headers, methods, origins)(router)

	log.Fatal(http.ListenAndServe(":3000", corsRouter))
}
