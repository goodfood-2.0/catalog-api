package controllers

import (
	"catalog-api/models"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type IngredientController struct {
	DB *gorm.DB
}

// @Summary Get all ingredients
// @Description Get all ingredients
// @Tags ingredients
// @Accept  json
// @Produce  json
// @Param page query int false "Page number"
// @Param limit query int false "Limit per page"
// @Param id query number false "Id of ingredient"
// @Param ids query string false "Ids of ingredients (comma separated)"
// @Param name query string false "Name of ingredient"
// @Param is_allergen query boolean false "Is allergen of ingredient"
// @Param is_allergen_description query string false "Is allergen description of ingredient"
// @Success 200 {object} models.GetAllResponse
// @Router /ingredients [get]
func (c *IngredientController) GetAllIngredientsHandler(w http.ResponseWriter, r *http.Request) {
	// parse query parameters and pagination parameters from request
	query := map[string]string{}
	queryParams := r.URL.Query()

	for key, value := range queryParams {
		if key == "page" || key == "limit" {
			continue
		}
		query[key] = value[0]
	}

	page, limit := 1, 10
	Page := queryParams.Get("page")
	Limit := queryParams.Get("limit")

	if Page != "" {
		page, _ = strconv.Atoi(Page)
		if page < 1 {
			page = 1
		}
	}

	if Limit != "" {
		limit, _ = strconv.Atoi(Limit)
		if limit < 1 {
			limit = 1
		}
	}

	ingredients, count, err := (&models.Ingredient{DB: c.DB}).GetAll(query, page, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create response object with pagination information and write to response
	response := map[string]interface{}{
		"count": count,
		"page":  page,
		"limit": limit,
		"data":  ingredients,
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// @Summary Get ingredient by id
// @Description Get ingredient by id
// @Tags ingredients
// @Accept  json
// @Produce  json
// @Param id path int true "Id of ingredient"
// @Success 200 {object} models.IngredientResponse
// @Router /ingredients/{id} [get]
func (c *IngredientController) GetIngredientHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	ingredient := &models.Ingredient{DB: c.DB}
	ingredientId, _ := strconv.Atoi(id)
	ingredient.ID = uint(ingredientId)

	err := ingredient.GetByID(ingredient.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ingredient)
}

// @Summary Create ingredient
// @Description Create ingredient
// @Tags ingredients
// @Accept  json
// @Produce  json
// @Param ingredient body models.IngredientResponse true "Ingredient object"
// @Success 200 {object} models.IngredientResponse
// @Router /ingredients [post]
func (c *IngredientController) CreateIngredientHandler(w http.ResponseWriter, r *http.Request) {
	ingredient := &models.Ingredient{DB: c.DB}
	json.NewDecoder(r.Body).Decode(&ingredient)

	err := ingredient.Create()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ingredient)
}

// @Summary Update ingredient
// @Description Update ingredient
// @Tags ingredients
// @Accept  json
// @Produce  json
// @Param id path int true "Id of ingredient"
// @Param ingredient body models.IngredientResponse true "Ingredient object"
// @Success 200 {object} models.IngredientResponse
// @Router /ingredients/{id} [put]
func (c *IngredientController) UpdateIngredientHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	if id == "" {
		http.Error(w, "Missing id", http.StatusBadRequest)
		return
	}

	// Get the ingredient by ID
	ingredient := &models.Ingredient{DB: c.DB}
	ingredientId, _ := strconv.Atoi(id)
	if err := ingredient.GetByID(uint(ingredientId)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Decode the request body into a map of updates
	var updates map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&updates); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Update only the fields present in the request body
	for key, value := range updates {
		switch key {
		case "name":
			ingredient.Name = value.(string)
		case "is_allergen":
			ingredient.IsAllergen = value.(bool)
		case "is_allergen_description":
			ingredient.IsAllergenDescription = value.(string)
		}
	}

	// Save the updated ingredient to the database
	if err := ingredient.Update(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the updated ingredient
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ingredient)
}

// @Summary Delete ingredient
// @Description Delete ingredient
// @Tags ingredients
// @Accept  json
// @Produce  json
// @Param id path int true "Id of ingredient"
// @Success 200
// @Router /ingredients/{id} [delete]
func (c *IngredientController) DeleteIngredientHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	ingredient := &models.Ingredient{DB: c.DB}
	ingredientId, _ := strconv.Atoi(id)
	ingredient.ID = uint(ingredientId)

	err := ingredient.Delete()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
