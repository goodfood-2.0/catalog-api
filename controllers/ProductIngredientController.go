package controllers

import (
	"catalog-api/models"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type ProductIngredientController struct {
	DB *gorm.DB
}

// @Summary Get all product ingredients
// @Description Get all product ingredients
// @Tags product-ingredients
// @Accept  json
// @Produce  json
// @Param page query int false "Page number"
// @Param limit query int false "Limit per page"
// @Param id_product query number false "Id of product"
// @Param ids_product query string false "Ids of products (comma separated)"
// @Param id_ingredient query number false "Id of ingredient"
// @Param ids_ingredient query string false "Ids of ingredients (comma separated)"
// @Param required_quantity query number false "Required quantity of ingredient"
// @Success 200 {object} models.GetAllResponse
// @Router /product-ingredients [get]
func (c *ProductIngredientController) GetAllProductIngredientsHandler(w http.ResponseWriter, r *http.Request) {
	// parse query parameters and pagination parameters from request
	query := map[string]string{}
	queryParams := r.URL.Query()

	for key, value := range queryParams {
		if key == "page" || key == "limit" {
			continue
		}
		query[key] = value[0]
	}

	page, limit := 1, 10
	Page := queryParams.Get("page")
	Limit := queryParams.Get("limit")

	if Page != "" {
		page, _ = strconv.Atoi(Page)
		if page < 1 {
			page = 1
		}
	}

	if Limit != "" {
		limit, _ = strconv.Atoi(Limit)
		if limit < 1 {
			limit = 1
		}
	}

	// call GetAll method on Restaurant model
	productIngredients, count, err := (&models.ProductIngredient{DB: c.DB}).GetAll(query, page, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create response object with pagination information and write to response
	response := map[string]interface{}{
		"count": count,
		"page":  page,
		"limit": limit,
		"data":  productIngredients,
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// @Summary Create product ingredient
// @Description Create product ingredient
// @Tags product-ingredients
// @Accept  json
// @Produce  json
// @Param product path int true "Id of product"
// @Param ingredient path int true "Id of ingredient"
// @Param required_quantity body number true "Required quantity of ingredient"
// @Success 200
// @Router /product-ingredients/{product}/ingredient/{ingredient} [post]
func (c *ProductIngredientController) CreateProductIngredientHandler(w http.ResponseWriter, r *http.Request) {
	productIngredient := &models.ProductIngredient{DB: c.DB}
	json.NewDecoder(r.Body).Decode(&productIngredient)

	id_product := mux.Vars(r)["product"]
	id_ingredient := mux.Vars(r)["ingredient"]

	IDProduct, _ := strconv.Atoi(id_product)
	IDIngredient, _ := strconv.Atoi(id_ingredient)
	productIngredient.IDProduct = uint(IDProduct)
	productIngredient.IDIngredient = uint(IDIngredient)

	err := productIngredient.GetById(productIngredient.IDProduct, productIngredient.IDIngredient)
	if err == nil {
		http.Error(w, "Relation already exists", http.StatusBadRequest)
		return
	}

	err = productIngredient.Create()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(productIngredient)
}

// @Summary Delete product ingredient by id
// @Description Delete product ingredient by id
// @Tags product-ingredients
// @Accept  json
// @Produce  json
// @Param product path int true "Id of product"
// @Param ingredient path int true "Id of ingredient"
// @Success 200
// @Router /product-ingredients/{product}/ingredient/{ingredient} [delete]
func (c *ProductIngredientController) DeleteProductIngredientHandler(w http.ResponseWriter, r *http.Request) {
	// how to get values from utl like /restaurant/1/supplier/1
	vars := mux.Vars(r)
	productID := vars["product"]
	ingredientID := vars["ingredient"]

	productIngredient := &models.ProductIngredient{DB: c.DB}
	IDProduct, _ := strconv.Atoi(productID)
	IDIngredient, _ := strconv.Atoi(ingredientID)
	productIngredient.IDProduct = uint(IDProduct)
	productIngredient.IDIngredient = uint(IDIngredient)

	// Check if the restaurant supplier exists
	err := productIngredient.GetById(productIngredient.IDProduct, productIngredient.IDIngredient)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Delete the restaurant supplier from the database
	if err := productIngredient.Delete(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
