package controllers

import (
	"catalog-api/azure"
	"catalog-api/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type ProductController struct {
	DB             *gorm.DB
	BlobServiceURL azblob.ServiceURL
}

// @Summary Get all products
// @Description Get all products
// @Tags products
// @Accept  json
// @Produce  json
// @Param page query int false "Page number"
// @Param limit query int false "Limit per page"
// @Param with query string false "Include ingredients (with=ingredients)"
// @Param id query number false "Id of product"
// @Param ids query string false "Ids of products (comma separated)"
// @Param name query string false "Name of product"
// @Param description query string false "Description of product"
// @Param price query number false "Price of product"
// @Param activated query boolean false "Activated of product"
// @Param id_restaurant query number false "Id of restaurant"
// @Success 200 {object} models.GetAllResponse
// @Router /products [get]
func (c *ProductController) GetAllProductsHandler(w http.ResponseWriter, r *http.Request) {
	// parse query parameters and pagination parameters from request
	query := map[string]string{}
	queryParams := r.URL.Query()
	withIngredients := queryParams.Get("with") == "ingredients"

	for key, value := range queryParams {
		if key == "page" || key == "limit" {
			continue
		}
		query[key] = value[0]
	}

	page, limit := 1, 10
	Page := queryParams.Get("page")
	Limit := queryParams.Get("limit")

	if Page != "" {
		var err error
		page, err = strconv.Atoi(Page)
		if err != nil || page < 1 {
			http.Error(w, "Invalid page parameter", http.StatusBadRequest)
			return
		}
	}

	if Limit != "" {
		var err error
		limit, err = strconv.Atoi(Limit)
		if err != nil || limit < 1 {
			http.Error(w, "Invalid limit parameter", http.StatusBadRequest)
			return
		}
	}

	products, count, err := (&models.Product{DB: c.DB}).GetAll(query, page, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	formattedProducts := make([]models.ClientProductResponse, len(products))
	for i, product := range products {

		formattedProduct := models.ClientProductResponse{
			ID:           product.ID,
			Name:         product.Name,
			Description:  product.Description,
			Price:        product.Price,
			Activated:    product.Activated,
			Image:        product.Image,
			IDRestaurant: product.IDRestaurant,
		}

		// Check if the "with" parameter is present and equals "ingredients"
		if withIngredients {
			var productIngredients []models.ProductIngredient
			if err := c.DB.Where("id_product = ?", product.ID).Find(&productIngredients).Error; err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			for _, productIngredient := range productIngredients {

				var ingredient models.Ingredient
				if err := c.DB.First(&ingredient, productIngredient.IDIngredient).Error; err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				formatedIngredients := models.ClientIngredientResponse{
					Name:                  ingredient.Name,
					IsAllergen:            ingredient.IsAllergen,
					IsAllergenDescription: ingredient.IsAllergenDescription,
					RequiredQuantity:      productIngredient.RequiredQuantity,
				}

				formattedProduct.Ingredients = append(formattedProduct.Ingredients, formatedIngredients)
			}
		}

		formattedProducts[i] = formattedProduct
	}

	// create response object with pagination information and write to response
	response := map[string]interface{}{
		"count": count,
		"page":  page,
		"limit": limit,
		"data":  formattedProducts,
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// @Summary Get product by id
// @Description Get product by id
// @Tags products
// @Accept  json
// @Produce  json
// @Param id path int true "Id of product"
// @Success 200 {object} models.ProductResponse
// @Router /products/{id} [get]
func (c *ProductController) GetProductHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	product := &models.Product{DB: c.DB}
	productId, _ := strconv.Atoi(id)
	product.ID = uint(productId)

	err := product.GetByID(product.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)
}

// @Summary Create product
// @Description Create product with an optional image upload
// @Tags products
// @Accept  json
// @Produce  json
// @Param product body models.ProductResponse true "Product object"
// @Param image formData file true "Product image"
// @Success 200 {object} models.ProductResponse
// @Router /products [post]
func (c *ProductController) CreateProductHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(10 << 20) // 10 MB limit
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	product := &models.Product{DB: c.DB}
	name := r.FormValue("name")
	description := r.FormValue("description")
	priceStr := r.FormValue("price")
	activatedStr := r.FormValue("activated")
	idRestaurantStr := r.FormValue("id_restaurant")
	price, _ := strconv.ParseFloat(priceStr, 64)
	activated, _ := strconv.ParseBool(activatedStr)
	idRestaurant, _ := strconv.Atoi(idRestaurantStr)

	product.Name = name
	product.Description = description
	product.Price = price
	product.Activated = activated
	product.IDRestaurant = idRestaurant

	imageFile, _, err := r.FormFile("image")
	if err == nil && os.Getenv("STORAGE_ACCOUNT_KEY") != "" && os.Getenv("STORAGE_ACCOUNT") != "" {
		defer imageFile.Close()

		// Read the image file into bytes
		imageBytes, err := ioutil.ReadAll(imageFile)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Generate a unique image file name (if desired)
		timestamp := time.Now().UnixNano()
		imageFileName := fmt.Sprintf("image_%d.jpg", timestamp)

		// Upload image to Azure Storage (or your preferred storage)
		blobUrl, err := azure.UploadImageToAzureStorage(c.BlobServiceURL, "images", imageFileName, imageBytes)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Update the product's image file name
		product.Image = blobUrl.String()
	}
	log.Println(err)

	err = product.Create()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)
}

// @Summary Update product
// @Description Update product
// @Tags products
// @Accept  json
// @Produce  json
// @Param id path int true "Id of product"
// @Param product body models.ProductResponse true "Product object"
// @Param image formData file false "Product image"
// @Success 200 {object} models.ProductResponse
// @Router /products/{id} [put]
func (c *ProductController) UpdateProductHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("UpdateProductHandler STEP 1")
	err := r.ParseMultipartForm(10 << 20) // 10 MB limit
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Println("UpdateProductHandler STEP 2")
	id := mux.Vars(r)["id"]
	if id == "" {
		http.Error(w, "Missing id", http.StatusBadRequest)
		return
	}

	log.Println("UpdateProductHandler STEP 3")
	// Get the product by ID
	product := &models.Product{DB: c.DB}
	productId, _ := strconv.Atoi(id)
	if err := product.GetByID(uint(productId)); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Println("UpdateProductHandler STEP 4")
	// Decode the request body into a map of updates
	if r.FormValue("name") != "" {
		product.Name = r.FormValue("name")
	}

	if r.FormValue("description") != "" {
		product.Description = r.FormValue("description")
	}

	if r.FormValue("price") != "" {
		priceStr := r.FormValue("price")
		price, _ := strconv.ParseFloat(priceStr, 64)
		product.Price = price
	}

	if r.FormValue("activated") != "" {
		activatedStr := r.FormValue("activated")
		activated, _ := strconv.ParseBool(activatedStr)
		product.Activated = activated
	}

	if r.FormValue("id_restaurant") != "" {
		idRestaurantStr := r.FormValue("id_restaurant")
		idRestaurant, _ := strconv.Atoi(idRestaurantStr)
		product.IDRestaurant = idRestaurant
	}

	// Check if an image is being uploaded
	if r.MultipartForm != nil {
		imageFile, _, err := r.FormFile("image")
		if err == nil {
			defer imageFile.Close()

			// Read the image file into bytes
			imageBytes, err := ioutil.ReadAll(imageFile)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			// Generate a unique image file name (if desired)
			timestamp := time.Now().UnixNano()
			imageFileName := fmt.Sprintf("image_%d.jpg", timestamp)

			// Upload image to Azure Storage (or your preferred storage)
			blobUrl, err := azure.UploadImageToAzureStorage(c.BlobServiceURL, "images", imageFileName, imageBytes)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			// Update the product's image file name
			product.Image = blobUrl.String()
		}
	}

	// Save the updated product to the database
	if err := product.Update(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the updated product
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(product)
}

// @Summary Delete product
// @Description Delete product
// @Tags products
// @Accept  json
// @Produce  json
// @Param id path int true "Id of product"
// @Success 200
// @Router /products/{id} [delete]
func (c *ProductController) DeleteProductHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	product := &models.Product{DB: c.DB}
	productId, _ := strconv.Atoi(id)
	product.ID = uint(productId)

	err := product.Delete()
	if err != nil {
		json.NewEncoder(w).Encode(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
