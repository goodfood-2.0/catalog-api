package azure

import (
	"context"
	"log"
	"net/url"
	"os"

	"github.com/Azure/azure-storage-blob-go/azblob"
)

func UploadImageToAzureStorage(blobServiceURL azblob.ServiceURL, containerName, blobName string, imageBytes []byte) (azblob.BlockBlobURL, error) {
	log.Println("Uploading image to Azure Storage...")
	containerURL := blobServiceURL.NewContainerURL(containerName)
	log.Println("containerURL: " + containerURL.String())
	blobURL := containerURL.NewBlockBlobURL(blobName)
	log.Println("blobURL: " + blobURL.String())

	log.Println("Uploading image to Azure Storage...")
	_, err := azblob.UploadBufferToBlockBlob(context.Background(), imageBytes, blobURL, azblob.UploadToBlockBlobOptions{
		BlobHTTPHeaders: azblob.BlobHTTPHeaders{
			ContentType: "image/jpeg", // Update with the appropriate content type
		},
	})
	log.Println("Image uploaded to Azure Storage")

	return blobURL, err
}

func InitializeAzureBlobServiceURL() azblob.ServiceURL {
	log.Println("Initializing Azure Blob Service URL...")
	log.Println("STORAGE_ACCOUNT: " + os.Getenv("STORAGE_ACCOUNT"))
	log.Println("STORAGE_ACCOUNT_KEY: " + os.Getenv("STORAGE_ACCOUNT_KEY"))
	accountName := os.Getenv("STORAGE_ACCOUNT")
	accountKey := os.Getenv("STORAGE_ACCOUNT_KEY")

	// Create a credential object
	credential, err := azblob.NewSharedKeyCredential(accountName, accountKey)
	if err != nil {
		panic("Failed to create credential")
	}

	// Create a pipeline using your credential
	pipeline := azblob.NewPipeline(credential, azblob.PipelineOptions{})

	// Create a URL object for the blob service endpoint
	URL, _ := url.Parse("https://" + accountName + ".blob.core.windows.net")

	// Get a ServiceURL object that wraps the service URL
	serviceURL := azblob.NewServiceURL(*URL, pipeline)

	return serviceURL
}
