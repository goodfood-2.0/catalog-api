package models

import (
	"strings"

	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	Price        float64       `json:"price"`
	Activated    bool          `json:"activated"`
	Image        string        `json:"image"`
	IDRestaurant int           `json:"id_restaurant"`
	DB           *gorm.DB      `gorm:"-" json:"-"`
}

func (p Product) TableName() string {
	return "products"
}

func (p *Product) Create() error {
	return p.DB.Create(p).Error
}

func (p *Product) GetByID(id uint) error {
	return p.DB.First(p, id).Error
}

func (p *Product) Update() error {
	return p.DB.Save(p).Error
}

func (p *Product) Delete() error {
	return p.DB.Delete(p).Error
}

func (p *Product) PermaDelete() error {
	return p.DB.Delete(p).Error
}

func (p *Product) GetAll(query map[string]string, page, limit int) ([]Product, int64, error) {
	var count int64
	var products []Product
	queryBuilder := p.DB.Model(&Product{})

	// Add filtering based on query parameters
	if len(query) > 0 {
		for key, value := range query {
			if strings.Contains(key, "with") {
				continue
			} else if strings.Contains(key, "ids") {
				key = strings.Replace(key, "ids", "id", 1)
				split := strings.Split(value, ",")
				queryBuilder = queryBuilder.Where(key+" IN (?)", split)
			} else if strings.Contains(key, "id") {
				queryBuilder = queryBuilder.Where(key+" = ?", value)
			} else {
				queryBuilder = queryBuilder.Where("LOWER("+key+") LIKE ?", "%"+strings.ToLower(value)+"%")
			}
		}
	}

	// Get total count of filtered records
	queryBuilder.Count(&count)

	// Add pagination
	offset := (page - 1) * limit
	queryBuilder = queryBuilder.Offset(offset).Limit(limit)

	err := queryBuilder.Find(&products).Error
	if err != nil {
		return nil, 0, err
	}

	return products, count, nil
}
