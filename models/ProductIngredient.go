package models

import (
	"strings"

	"gorm.io/gorm"
)

type ProductIngredient struct {
	IDProduct        uint     `gorm:"primaryKey" json:"id_product"`
	IDIngredient     uint     `gorm:"primaryKey" json:"id_ingredient"`
	RequiredQuantity int      `json:"required_quantity"`
	DB               *gorm.DB `gorm:"-" json:"-"`
}

func (s ProductIngredient) TableName() string {
	return "product-ingredients"
}

func (is *ProductIngredient) Create() error {
	return is.DB.Create(is).Error
}

func (is *ProductIngredient) GetById(productID uint, ingredientID uint) error {
	return is.DB.Where("id_product = ? AND id_ingredient = ?", productID, ingredientID).First(is).Error
}

func (is *ProductIngredient) Update() error {
	return is.DB.Save(is).Error
}

func (is *ProductIngredient) Delete() error {
	return is.DB.Delete(is).Error
}

func (is *ProductIngredient) GetAll(query map[string]string, page, limit int) ([]ProductIngredient, int64, error) {
	var count int64
	var productIngredients []ProductIngredient
	queryBuilder := is.DB

	// Add filtering based on query parameters
	if len(query) > 0 {
		for key, value := range query {
			if strings.Contains(key, "ids") {
				key = strings.Replace(key, "ids", "id", 1)
				split := strings.Split(value, ",")
				queryBuilder = queryBuilder.Where(key+" IN (?)", split)
			} else if strings.Contains(key, "id") {
				queryBuilder = queryBuilder.Where(key+" = ?", value)
			} else {
				queryBuilder = queryBuilder.Where("LOWER("+key+") LIKE ?", "%"+strings.ToLower(value)+"%")
			}
		}
	}

	println(queryBuilder)

	// Get total count of filtered records
	queryBuilder.Model(&ProductIngredient{}).Count(&count)

	// Add pagination
	offset := (page - 1) * limit
	queryBuilder = queryBuilder.Offset(offset).Limit(limit)

	err := queryBuilder.Find(&productIngredients).Error
	if err != nil {
		return nil, 0, err
	}

	return productIngredients, count, nil
}
