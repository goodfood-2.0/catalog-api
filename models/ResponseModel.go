package models

type ProductResponse struct {
	ID           uint    `json:"id"`
	Name         string  `json:"name"`
	Description  string  `json:"description"`
	Price        float64 `json:"price"`
	Activated    bool    `json:"activated"`
	Image        string  `json:"image"`
	IDRestaurant int     `json:"id_restaurant"`
}

type IngredientResponse struct {
	ID                    uint   `json:"id"`
	Name                  string `json:"name"`
	IsAllergen            bool   `json:"is_allergen"`
	IsAllergenDescription string `json:"is_allergen_description"`
}

type ProductIngredientResponse struct {
	IDProduct        uint `gorm:"primaryKey" json:"id_product"`
	IDIngredient     uint `gorm:"primaryKey" json:"id_ingredient"`
	RequiredQuantity int  `json:"required_quantity"`
}

type GetAllResponse struct {
	Count int           `json:"count"`
	Page  int           `json:"page"`
	Limit int           `json:"limit"`
	Data  []interface{} `json:"data"`
}

// CLIENT RESPONSE

type ClientProductResponse struct {
	ID           uint                       `json:"id"`
	Name         string                     `json:"name"`
	Description  string                     `json:"description"`
	Price        float64                    `json:"price"`
	Activated    bool                       `json:"activated"`
	Image        string                     `json:"image"`
	IDRestaurant int                        `json:"id_restaurant"`
	Ingredients  []ClientIngredientResponse `json:"ingredients"`
}

type ClientIngredientResponse struct {
	Name                  string `json:"name"`
	IsAllergen            bool   `json:"is_allergen"`
	IsAllergenDescription string `json:"is_allergen_description"`
	RequiredQuantity      int    `json:"required_quantity"`
}
