package models

import (
	"strings"

	"gorm.io/gorm"
)

type Ingredient struct {
	gorm.Model
	Name                  string   `json:"name"`
	IsAllergen            bool     `json:"is_allergen"`
	IsAllergenDescription string   `json:"is_allergen_description"`
	DB                    *gorm.DB `gorm:"-" json:"-"`
}

func (s Ingredient) TableName() string {
	return "ingredients"
}

func (r *Ingredient) Create() error {
	return r.DB.Create(r).Error
}

func (r *Ingredient) GetByID(id uint) error {
	return r.DB.First(r, id).Error
}

func (r *Ingredient) Update() error {
	return r.DB.Save(r).Error
}

func (r *Ingredient) Delete() error {
	return r.DB.Delete(r).Error
}

func (r *Ingredient) GetAll(query map[string]string, page, limit int) ([]Ingredient, int64, error) {
	var count int64
	var ingredients []Ingredient
	queryBuilder := r.DB

	// Add filtering based on query parameters
	if len(query) > 0 {
		for key, value := range query {
			if strings.Contains(key, "ids") {
				key = strings.Replace(key, "ids", "id", 1)
				split := strings.Split(value, ",")
				queryBuilder = queryBuilder.Where(key+" IN (?)", split)
			} else if strings.Contains(key, "id") {
				queryBuilder = queryBuilder.Where(key+" = ?", value)
			} else {
				queryBuilder = queryBuilder.Where("LOWER("+key+") LIKE ?", "%"+strings.ToLower(value)+"%")
			}
		}
	}

	// Get total count of filtered records
	queryBuilder.Model(&Ingredient{}).Count(&count)

	// Add pagination
	offset := (page - 1) * limit
	queryBuilder = queryBuilder.Offset(offset).Limit(limit)

	err := queryBuilder.Find(&ingredients).Error
	if err != nil {
		return nil, 0, err
	}

	return ingredients, count, nil
}
